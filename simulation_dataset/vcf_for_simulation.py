import argparse
from collections import defaultdict
import random
from Bio import SeqIO
import os
import re


BASES_DICT = {'A': 0, 0: 'A', 'C': 1, 1: 'C', 'G': 2, 2: 'G', 'T': 3, 3: 'T'}
random.seed(23121989)

def get_alt_base(base):
    if base == "N":
        alt = BASES_DICT[random.randint(0, 3)]
    else:
        alt = BASES_DICT[(BASES_DICT[base] + random.randint(1, 3)) % 4]
    return alt


def main():
    global args

    path = "coverage_vcfs/"

    # get sample info
    smp_dict = defaultdict(lambda: defaultdict(list))
    cluster_dict = defaultdict(list)
    with open(args.smp_file, "r") as inf:
        line = inf.readline()  # skip first line
        for line in inf:
            fields = line.strip().split("\t")
            cluster = int(fields[0])
            sample = fields[1]
            snp_type = fields[2]

            if fields[3] != "-":
                snp_nr = int(fields[3])
                smp_dict[sample][snp_type].append(snp_nr)
                smp_dict[sample]["cluster"] = cluster

            cluster_dict[cluster].append(sample)


    # read smp file (coordinates one-based)
    genome_dict = defaultdict(lambda: defaultdict(set))
    snp_dict = defaultdict(lambda: defaultdict(lambda: defaultdict(dict)))

    with open(args.snp_file, "r") as inf:
        line = inf.readline() # skip header
        for line in inf:

            fields = line.strip().split("\t")
            genome_name = fields[1]
            genome_acc = fields[2]
            genome_path = fields[3]
            genome_dict[genome_name]["acc"] = genome_acc
            genome_dict[genome_name]["path"] = genome_path

            cluster = int(fields[0])
            snp_type = fields[5]
            pos = int(fields[6])
            pos_nr = int(fields[7])
            snp_dict[cluster][snp_type][pos_nr] = {"pos": pos}
            snp_dict[cluster]["genome"] = genome_name

            genome_dict[genome_name]["clusters"].add(cluster)

    # read deletion file (coordinates zero-base)
    del_dict = defaultdict(list)
    with open(args.del_file, "r") as inf:
        for line in inf:
            fields = line.strip().split("\t")
            sample = fields[0]
            start = int(fields[1])  # zero-based incl [start, end[
            end = int(fields[2])    # zero-based excl [start, end[

            del_dict[sample].append({"start": start, "end": end})

    # vcf coordinates one-based
    # python string zero-based

    # get base info
    for genome in sorted(genome_dict.keys()):
        # parse FASTA
        records = list(SeqIO.parse(genome_dict[genome]["path"], "fasta"))
        seq = records[0].seq

        for cluster in sorted(genome_dict[genome]["clusters"]):
            for snp_type in ("inner", "outer"):
                # get REF and generate ALT for all positions
                for pos_nr in sorted(snp_dict[cluster][snp_type]):
                    pos = snp_dict[cluster][snp_type][pos_nr]["pos"]
                    # coord one-based
                    ref = seq[pos - 1]
                    ref = re.sub("[^ACGT]", "N", ref)
                    alt = get_alt_base(ref)
                    ref = re.sub("N", "A", ref)
                    snp_dict[cluster][snp_type][pos_nr]["ref"] = ref
                    snp_dict[cluster][snp_type][pos_nr]["alt"] = alt

            for sample in cluster_dict[cluster]:
                for i in range(0, len(del_dict[sample])):
                    region = del_dict[sample][i]
                    ref = str(seq[(region["start"]):region["end"]])
                    ref = re.sub("[^ACGT]", "N", ref)
                    alt = ref[0]
                    ref = re.sub("N", "A", ref)
                    del_dict[sample][i]["ref"] = ref
                    del_dict[sample][i]["alt"] = alt

        # also create output folder per genome
        os.makedirs(path + genome.replace("/", "_"), exist_ok=True)


    # special NEAT simulator format:
    vcf_header = "##fileformat=VCFv4.2\n" \
                 "##INFO=<ID=WP,Number=A,Type=Integer,Description=\"NEAT-GenReads ploidy indicator\">\n" \
                 "#CHROM\tPOS\tID\tREF\tALT\tQUAL\tFILTER\tINFO\n"
    line_format = "{}\t{!s}\t.\t{}\t{}\t.\t.\tWP=1/1\n"

    for sample, info in smp_dict.items():
        cluster = info["cluster"]
        genome_name = snp_dict[cluster]["genome"]
        genome_acc = genome_dict[genome_name]["acc"]

        # write SNP vcfs
        with open(path + genome_name.replace("/", "_") + "/" +
                  "_".join(["simulation_C" + str(cluster), "S" + sample]) + "_allele.vcf", "w") as outf1:
            outf1.write(vcf_header)
            for snp_type in ("outer", "inner"):
                for snp_nr in info[snp_type]:
                    snp_info = snp_dict[cluster][snp_type][snp_nr]
                    pos = snp_info["pos"]
                    ref = snp_info["ref"]
                    alt = snp_info["alt"]
                    outf1.write(line_format.format(genome_acc, pos, ref, alt))

        # write deletion vcf
        with open(path + genome_name.replace("/", "_") + "/" +
                  "_".join(["simulation_C" + str(cluster), "S" + sample]) + "_deletions.vcf", "w") as outf:
            outf.write(vcf_header)
            for region in del_dict[sample]:
                pos = region["start"]+1
                ref = region["ref"]
                alt = region["alt"]
                outf.write(line_format.format(genome_acc, pos, ref, alt))


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--input_smp", dest="smp_file", help="Sample input file.", required=True)
    parser.add_argument("--input_snp", dest="snp_file", help="Sample SNP file.", required=True)
    parser.add_argument("--input_coverage", dest="del_file", help="Sample Coverage BED File", required=True)

    args = parser.parse_args()
    main()