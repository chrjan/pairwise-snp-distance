import glob
import re
import math
import subprocess


## Globals
configfile: "configfile.txt"

GENOME = config["genome_file"]
GENOME_FDIR = os.path.dirname(GENOME)
GENOME = os.path.splitext(os.path.basename(GENOME))[0]
FASTQDIR = config.get("fastqdir", "fastq")
FASTQPOSTFIX=config.get("fastqpostfix", "")


SAMPLE_LIST = config["samples_list"]
with open(SAMPLE_LIST, "r") as samp_f:
    SAMPLES = [ x.strip() for x in samp_f.readlines()]
    SAMPLES = list(x for x in SAMPLES if x)


FLASH_OVERLAP = config.get("flash_overlap", 10)
READ_MIN_LEN = config.get("trimmomatic_read_minimum_length", 50)
ADAPTER_FILE= config.get("trimmomatic_adapter_file", srcdir("Trimmomatic_adapter/NexteraPE-PE.fa"))
MIN_QUAL = config.get("trimmomatic_qual_slidingwindow", 15)


AF_threshold = config.get("allele_frequency_threshold", 0.75)
MQ_threshold = config.get("mapping_quality_threshold", 10)
DP_threshold = config.get("depth_threshold", 5)


CLIPPING = FLASH_OVERLAP/2
PYTHON_SCRIPTS=srcdir("scripts/")

def readlength_from_file(wildcards):
    shell_cmd = " | awk 'BEGIN{ml=0}{if(NR%4==2){ x=length($1);  if (x+0>ml+0) ml=x}} END{print ml}'"
    rl1 = int(subprocess.check_output("gzip -dc adapt_clip/" + wildcards['smp'] + "/" + wildcards['smp'] + "_1.fastq.gz" + shell_cmd, shell=True))
    rl2 = int(subprocess.check_output("gzip -dc adapt_clip/" + wildcards['smp'] + "/" + wildcards['smp'] + "_2.fastq.gz" + shell_cmd, shell=True))
    return max(rl1, rl2)

def readlength_from_name(wildcards):
    m = re.search('(\d+)bp', wildcards['smp'])
    if m is not None:
        rl = int(m.group(1))
    else:
        rl = readlength_from_file(wildcards)
    rl_clip = rl - CLIPPING
    return int(rl), int(rl_clip)


def readlength_from_name_70(wildcards):
    rl = readlength_from_name(wildcards)
    rl_70 = math.floor(rl[0] * 0.7)
    return int(rl_70)
    

def indexNbases_from_fai(fai_file):
    with open(fai_file, 'r') as f:
        first_line = f.readline()
        gl = int(first_line.split('\t')[1])
        gl = math.floor((math.log2(gl)/2) - 1.5)
        gl = min(14,gl)
        return int(gl)

       
rule all:
    input:
        expand("ready_forvar/{smp}/{genome}/{smp}_ready.bam", smp=SAMPLES, genome=GENOME),
        expand("SNP_calling/{smp}/{genome}/{smp}_vars_indels.vcf", smp=SAMPLES, genome=GENOME),
        expand("SNP_calling/{smp}/{genome}/{smp}_vars_snps.vcf", smp=SAMPLES, genome=GENOME),
        expand("SNP_calling/{smp}/{genome}/{smp}_vars_svs.vcf", smp=SAMPLES, genome=GENOME),
        expand("SNP_calling/{smp}/{genome}/{smp}_uncovered.bed", smp=SAMPLES, genome=GENOME),
        expand("SNP_calling/{smp}/{genome}/{smp}_uncalled.bed", smp=SAMPLES, genome=GENOME),
        expand("SNP_calling/{smp}/{genome}/{smp}_vars_deletions.bed", smp=SAMPLES, genome=GENOME),
        expand("SNP_calling/{smp}/{genome}/{smp}_vars_snps_amb-lowqual.bed", smp=SAMPLES, genome=GENOME),
        expand("SNP_calling/{smp}/{genome}/{smp}_vars_snps_lowqual.bed", smp=SAMPLES, genome=GENOME)
        
        
############### transition steps      
        
rule merge_reads:
    input:
        fq1="adapt_clip/{smp}/{smp}_1.fastq.gz",
        fq2="adapt_clip/{smp}/{smp}_2.fastq.gz"
    output:
        temp("merged/{smp}/{smp}.extendedFrags.fastq.gz"),
        temp("merged/{smp}/{smp}.notCombined_1.fastq.gz"),
        temp("merged/{smp}/{smp}.notCombined_2.fastq.gz"),
        temp("merged/{smp}/{smp}.hist"),
        temp("merged/{smp}/{smp}.histogram")
    params: sample="{smp}", overlap=FLASH_OVERLAP, outdir="merged/{smp}", max_overlap=lambda wildcards: readlength_from_name(wildcards)
    log: "merged/{smp}/flash.log"
    shell:
        "flash -t 1 -z -m {params.overlap} -M {params.max_overlap[0]} -d {params.outdir} -o {params.sample} {input.fq1} {input.fq2} 2>&1 > {log}"


rule prepare_mapping:
    input:  
        fq1up="QCPE/{smp}/r1_UP_{smp}.notCombined_clipped_1.fastq",
        fq2up="QCPE/{smp}/r2_UP_{smp}.notCombined_clipped_2.fastq",
        fq1p="QCPE/{smp}/r1_P_{smp}.notCombined_clipped_1.fastq",
        fq2p="QCPE/{smp}/r2_P_{smp}.notCombined_clipped_2.fastq",
        fqm="QCSE/{smp}/{smp}.extendedFrags.fastq",
    output:
        fq1="ready/{smp}/{smp}.pe_1.fastq.gz",
        fq2="ready/{smp}/{smp}.pe_2.fastq.gz",
        fqm="ready/{smp}/{smp}.se.fastq.gz"
    shell:
        """
        gzip -c {input.fq1p} > {output.fq1}
        gzip -c  {input.fq2p} > {output.fq2}
        cat {input.fqm} {input.fq1up} {input.fq2up} | gzip -c > {output.fqm}
        """        


############# quality control       
        
rule qc_pe:
    input:
        notcomb1="clipping/{smp}/{smp}.notCombined_clipped_1.fastq",
        notcomb2="clipping/{smp}/{smp}.notCombined_clipped_2.fastq"
    output:
        p1=temp("QCPE/{smp}/r1_P_{smp}.notCombined_clipped_1.fastq"),
        up1=temp("QCPE/{smp}/r1_UP_{smp}.notCombined_clipped_1.fastq"),
        p2=temp("QCPE/{smp}/r2_P_{smp}.notCombined_clipped_2.fastq"),
        up2=temp("QCPE/{smp}/r2_UP_{smp}.notCombined_clipped_2.fastq")
    threads: 4
    shell:
        "trimmomatic PE -threads {threads} -phred33 {input.notcomb1} {input.notcomb2} {output.p1} {output.up1} {output.p2} {output.up2} SLIDINGWINDOW:4:15 MINLEN:{READ_MIN_LEN}"

        
rule qc_se:
    input:
        extfraq="merged/{smp}/{smp}.extendedFrags.fastq.gz"
    output:
        temp("QCSE/{smp}/{smp}.extendedFrags.fastq"),
    threads: 4
    shell:
        "trimmomatic SE -threads {threads} -phred33 {input.extfraq} {output} SLIDINGWINDOW:4:{MIN_QUAL} MINLEN:{READ_MIN_LEN}"

        
############## clipping        
    
rule clip:
    input:
        "merged/{smp}/{smp}.notCombined_{nr, \d}.fastq.gz"
    output:
        temp("clipping/{smp}/{smp}.notCombined_clipped_{nr}.fastq")
    log: "clipping/{smp}/clip{nr}.log"
    params: clip=CLIPPING
    shell:
        "seqtk trimfq -e {params.clip} {input} > {output} 2> {log}"

        
rule adapter_clip:
    input:
        fq1=expand("{fastqdir}/{{smp}}_{fpostfix}1.fastq.gz", fastqdir=FASTQDIR, fpostfix=FASTQPOSTFIX),
        fq2=expand("{fastqdir}/{{smp}}_{fpostfix}2.fastq.gz", fastqdir=FASTQDIR, fpostfix=FASTQPOSTFIX),
        af=ADAPTER_FILE
    output:
        fq1=temp("adapt_clip/{smp}/{smp}_1.fastq.gz"),
        fq2=temp("adapt_clip/{smp}/{smp}_2.fastq.gz"),
        fqu1=temp("adapt_clip/{smp}/{smp}_1_up.fastq.gz"),
        fqu2=temp("adapt_clip/{smp}/{smp}_2_up.fastq.gz")
    log: "adapt_clip/{smp}/illuminaclip.log"
    shell:
        "trimmomatic PE -phred33 -threads 1 {input.fq1} {input.fq2} {output.fq1} {output.fqu1} {output.fq2} {output.fqu2} ILLUMINACLIP:{input.af}:2:30:10:1:true 2> {log}"
           

############## mapping

rule map_pe:
    input:
        fq1="ready/{smp}/{smp}.pe_1.fastq.gz",
        fq2="ready/{smp}/{smp}.pe_2.fastq.gz",
        index=GENOME_FDIR+"/{genome}.fasta.bwt",
        g=GENOME_FDIR+"/{genome}.fasta"
    output:
        f=temp("mapped/{smp}/{genome}/{smp}_pe_Aligned.out.bam")
    log: "mapped/{smp}/{genome}/{smp}_pe_bwamem.log"
    shell:
        "bwa mem {input.g} {input.fq1} {input.fq2} 2> {log} | samtools view -Sb - > {output}"


rule map_se:
    input:
        fq="ready/{smp}/{smp}.se.fastq.gz",
        index=GENOME_FDIR+"/{genome}.fasta.bwt",
        g=GENOME_FDIR+"/{genome}.fasta"
    output:
        f=temp("mapped/{smp}/{genome}/{smp}_se_Aligned.out.bam")
    log: "mapped/{smp}/{genome}/{smp}_se_bwamem.log"
    shell:
        "bwa mem {input.g} {input.fq} 2> {log} | samtools view -Sb - > {output}"


        
############### bam file modifications        

rule sort_bam:
    input:
        "mapped/{smp}/{genome}/{smp}_{reads}_Aligned.out.bam"
    output:
        temp("mapped/{smp}/{genome}/{smp}_{reads}_Aligned.sortedByCoord.out.bam")
    log:
       "mapped/{smp}/{genome}/{smp}_{reads}_Aligned.sortedByCoord.out.tmp"
    params: mem="5G"
    shell:
        "samtools sort -T {log} -m {params.mem} {input} > {output}"

        
rule combine_bam:
    input: 
        se="mapped/{smp}/{genome}/{smp}_se_Aligned.sortedByCoord.out.bam",
        pe="mapped/{smp}/{genome}/{smp}_pe_Aligned.sortedByCoord.out.bam"
    output:
        temp("ready_forvar/{smp}/{genome}/{smp}.bam")
    shell:
        "samtools merge {output} {input.se} {input.pe}"
        

        
rule mark_duplicates:
    input:
        "ready_forvar/{smp}/{genome}/{smp}.bam"
    output:
        bam=temp("ready_forvar/{smp}/{genome}/{smp}_markeddup.bam"),
        txt=temp("ready_forvar/{smp}/{genome}/{smp}_metrics.txt")
    log: "ready_forvar/{smp}/{genome}/mark_duplicates.log"
    threads:2
    shell:
        "picard MarkDuplicates I={input} O={output.bam} METRICS_FILE={output.txt} 2>&1> {log}"
        

rule add_readgroups:
    input:
        "ready_forvar/{smp}/{genome}/{smp}_markeddup.bam"
    output:
        "ready_forvar/{smp}/{genome}/{smp}_ready.bam"
    log: "ready_forvar/{smp}/{genome}/add_readgroups.log"
    threads:2
    run:
        sm = wildcards["smp"]
        lb = "lb"
        pu = "pu"
        shell("picard AddOrReplaceReadGroups I={input[0]} O={output} LB={lb} PL=illumina PU={pu} SM={sm} 2>&1> {log}")
        
        
        
############### indices        
rule fa_index:
    input: "{genome}"
    output: "{genome}.fai"
    shell: "samtools faidx {input}"

        
rule fasta_dict:
    input:
        "{fastafile}.fasta"
    output:
        "{fastafile}.dict"
    shell:
        "picard CreateSequenceDictionary R={input} O={output}"

        
rule bam_index:
    input:
        "{bamfile}.bam"
    output:
        "{bamfile}.bam.bai"
    shell:
        "samtools index {input}"


rule bwa_index:
    input:
        "{genome}"
    output:
        "{genome}.bwt",
        "{genome}.amb",
        "{genome}.ann",
        "{genome}.pac",
        "{genome}.sa"
    shell:
        "bwa index {input}"

                
rule tabix:
    input:
        "{vcffile}.vcf"
    output:
        gz="{vcffile}.vcf.gz",
        tbi="{vcffile}.vcf.gz.tbi"
    shell:
        """
        bgzip -c {input} > {output.gz}
        tabix {output.gz}
        """        
        
        
############### snpcalling

rule bam_filter:
    input:
        "{bamfile}.bam"
    output:
        temp("{bamfile}_mqfiltered.bam")
    shell:
        "samtools view -q {MQ_threshold} -b {input} > {output}"

rule snp_calling:
    input:
        fai=GENOME_FDIR + "/{genome}.fasta.fai",
        fasta=GENOME_FDIR + "/{genome}.fasta",
        dict=GENOME_FDIR + "/{genome}.dict",
        bam="ready_forvar/{smp}/{genome}/{smp}_ready.bam",
        bai="ready_forvar/{smp}/{genome}/{smp}_ready.bam.bai"
    output:
        vcf=temp("SNP_calling/{smp}/{genome}/{smp}.g.vcf"),
        idx=temp("SNP_calling/{smp}/{genome}/{smp}.g.vcf.idx")
    log: "SNP_calling/{smp}/{genome}/snpcalling.log"
    threads: 2
    shell:
        "gatk3 -T HaplotypeCaller -o {output.vcf} -I {input.bam} --sample_ploidy 2 -R {input.fasta} -mmq {MQ_threshold} -ERC GVCF &> {log}"


rule genotype_calling:
    input:
        gvcf="SNP_calling/{smp}/{genome}/{smp}.g.vcf",
        fai=GENOME_FDIR + "/{genome}.fasta.fai",
        fasta=GENOME_FDIR + "/{genome}.fasta",
        dict=GENOME_FDIR + "/{genome}.dict",
    output:
        vcf=temp("SNP_calling/{smp}/{genome}/{smp}.vcf"),
        idx=temp("SNP_calling/{smp}/{genome}/{smp}.vcf.idx")
    log: "SNP_calling/{smp}/{genome}/genotyping.log"
    shell:
        "gatk3 -T GenotypeGVCFs -R {input.fasta} --variant {input.gvcf} -o {output.vcf} --sample_ploidy 2 --includeNonVariantSites -stand_call_conf {DP_threshold} &> {log}"


rule gatk_vars:
    input:
        "SNP_calling/{smp}/{genome}/{smp}.vcf"
    output:
        vcf=temp("SNP_calling/{smp}/{genome}/{smp}_vars.vcf"),
        bed=temp("SNP_calling/{smp}/{genome}/{smp}_uncalled_singlepos.bed"),
    shell:
        "python3 {PYTHON_SCRIPTS}filter_gatk.py -i {input} --output_vcf {output.vcf} --output_bed {output.bed}"


rule uncalled:
    input: 
        "SNP_calling/{smp}/{genome}/{smp}_uncalled_singlepos.bed"
    output:
        "SNP_calling/{smp}/{genome}/{smp}_uncalled.bed"
    shell:
        "bedtools merge -i {input} > {output}"

        
rule genomecoverage:
    input:
        bam="ready_forvar/{smp}/{genome}/{smp}_ready_mqfiltered.bam",
        fasta=GENOME_FDIR + "/{genome}.fasta"
    output:
        temp("SNP_calling/{smp}/{genome}/{smp}_coverage.bed")
    shell:
        "bedtools genomecov -bga -ibam {input.bam} > {output}"

        
rule low_coverage:
    input: 
        "SNP_calling/{smp}/{genome}/{smp}_coverage.bed"
    output: 
        "SNP_calling/{smp}/{genome}/{smp}_uncovered.bed"
    shell:
        "awk '$4 < {DP_threshold}' {input} | bedtools merge -i - > {output}"


############### vcf filtering

rule split_vars:
    input:
        vcf="{vcffile}.vcf",
        dict=GENOME_FDIR + "/" + GENOME + ".dict",
        fai=GENOME_FDIR + "/" + GENOME + ".fasta.fai",
        fasta=GENOME_FDIR + "/" + GENOME + ".fasta"
    output:
        snps="{vcffile}_snps.vcf",
        indels="{vcffile}_indels.vcf",
        svs="{vcffile}_svs.vcf",
        snpsidx=temp("{vcffile}_snps.vcf.idx"),
        indelsidx=temp("{vcffile}_indels.vcf.idx"),
        svsidx=temp("{vcffile}_svs.vcf.idx"),
    threads: 1
    log: "{vcffile}.log"
    shell:
        """
        gatk3 -T SelectVariants -o {output.snps} -V {input.vcf} -selectType SNP -R {input.fasta} &> {log}
        gatk3 -T SelectVariants -o {output.indels} -V {input.vcf} -selectType INDEL -R {input.fasta} &>> {log}
        gatk3 -T SelectVariants -o {output.svs} -V {input.vcf} --selectTypeToExclude INDEL --selectTypeToExclude SNP -R {input.fasta} &>> {log}
        """


## deletions are reported on the previous base with "chr pos . AG A" --> $2 instead of $2-1
rule deletions_bed:
    input:
        "SNP_calling/{smp}/{genome}/{smp}_vars_indels.vcf"
    output:
        "SNP_calling/{smp}/{genome}/{smp}_vars_deletions.bed"
    shell:
        "grep -v '#' {input} | awk 'BEGIN{{OFS=\"\t\";}} {{if(length($4)>length($5)){{ print $1, $2, $2+(length($4)-length($5)) }}}}' > {output}"
        
        
        
##    special filters with python (AF calculation) "chr pos .  A  G" --> $2-1  (vcf is one-based, bed files are zero-based)    
rule snps_amb_lowqual_regions:
    input:
        "SNP_calling/{smp}/{genome}/{smp}_vars_snps.vcf"
    output:
        "SNP_calling/{smp}/{genome}/{smp}_vars_snps_amb-lowqual.bed"
    shell:
        "python3 {PYTHON_SCRIPTS}filter_af_gatk_bed.py -i {input} -o {output} -a {AF_threshold}"
        


##    GATK LowQual filter     
rule snps_lowqual_regions:
    input:
        "SNP_calling/{smp}/{genome}/{smp}_vars_snps.vcf"
    output:
        "SNP_calling/{smp}/{genome}/{smp}_vars_snps_lowqual.bed"
    shell:
        "python3 {PYTHON_SCRIPTS}filter_dp_gatk_bed.py -i {input} -o {output} -d {DP_threshold}"  
        
